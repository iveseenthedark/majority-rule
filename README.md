# Majority Rule - Democratize the trivial

Simple app for creating polls. 

## Development Setup

Uses Firebase for the back-end. To link this to your account create a json file `src/firebase.config.json` with a object containing your Firebase settings. 

```json
{
  "apiKey"            : "XXXXXXXXXXXXXXXXXXXXXXXX",
  "authDomain"        : "XXXXXXXXXXXXXXXXXXXXXXXX",
  "databaseURL"       : "XXXXXXXXXXXXXXXXXXXXXXXX",
  "projectId"         : "XXXXXXXXXXXXXXXXXXXXXXXX",
  "storageBucket"     : "XXXXXXXXXXXXXXXXXXXXXXXX",
  "messagingSenderId" : "XXXXXXXXXXXXXXXXXXXXXXXX"
}
```

## Generate Security Rules

To generate the security rules for the firebase database install `firebase-bolt` globally
```
npm install -g firebase-bolt
```
and then run from the root folder
```
firebase-bolt security-rules.bolt
```
This will generate a file `security-rules.json` that will contain the rules that you can copy into your firebase database settings