import React from 'react';

import { Router, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createHashHistory';

import { Provider, Box } from 'rebass';

import './Utils/global-styles';
import './Utils/font-library';
import theme from './Utils/theme';

import { SecureRoute, AuthRoute } from './Components/Route';

import Appbar from './Components/Appbar';
import Drawer from './Components/Drawer';

import PollNew from './Pages/PollNew';
import MyPolls from './Pages/MyPolls';
import PollGrid from './Pages/PollGrid';
import PollVote from './Pages/PollVote';
import PollResult from './Pages/PollResult';

import { SignIn, FinishSignUp } from './Pages/SignIn';

const history = createBrowserHistory();

class App extends React.Component {
	state = {
		drawOpen: false
	};

	componentWillMount = () => {
		this.unlisten = history.listen(() => {
			this.state.drawOpen && this.setState({ drawOpen: false });
		});
	};

	componentWillUnmount = () => {
		this.unlisten();
	};

	handleOpenDraw = () => {
		this.setState({ drawOpen: !this.state.drawOpen });
	};

	render() {
		const { drawOpen } = this.state;
		return (
			<Router history={history}>
				<Provider
					theme={theme}
					css={{ display: 'flex', flexDirection: 'column', paddingTop: '48px', minHeight: '100%' }}>
					<Appbar drawOpen={drawOpen} onToggleDraw={this.handleOpenDraw} />
					<Box is="main" flex="1 1 0">
						<Switch>
							{/* These routes require authentication */}
							<SecureRoute path="/" exact component={PollGrid} />
							<SecureRoute path="/mine" component={MyPolls} />
							<SecureRoute path="/vote/:key" component={PollVote} />
							<SecureRoute path="/result/:key" component={PollResult} />
							<SecureRoute path="/new" component={PollNew} />

							{/* These routes will redirect you back one you're logged in */}
							<AuthRoute path="/signin" component={SignIn} />
							<AuthRoute path="/fsu" component={FinishSignUp} />
						</Switch>
					</Box>
					<Drawer open={drawOpen} />
				</Provider>
			</Router>
		);
	}
}

export default App;
