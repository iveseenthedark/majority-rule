import firebase from '../Utils/firebase.js';
import Poll from '../Domain/Poll';

export default async ({ key }) => {
	const db = firebase.database();

	const poll_snap = await db.ref(`/polls/${key}`).once('value');
	const poll = poll_snap.val();

	const votes_snap = await db.ref(`/votes/${key}`).once('value');
	return new Poll(key, poll, votes_snap.val());
};
