import firebase from '../Utils/firebase.js';
import Poll from '../Domain/Poll';

export default async () => {
	const db = firebase.database();

	const poll_snap = await db
		.ref('/polls')
		.orderByChild('owner')
		.equalTo('yoi2qQgIgGaUE2KVogdYc9dPPAU2')
		.once('value');
	const polls = poll_snap.val();

	if (!polls) {
		console.log('NO POLLS!!!!');
		return null;
	}

	const polls_with_votes = [];
	for (let [key, poll] of Object.entries(polls)) {
		const votes_snap = await db.ref(`/votes/${key}`).once('value');
		polls_with_votes.push(new Poll(key, poll, votes_snap.val()));
	}

	polls_with_votes.sort((a, b) => b.tally_count() - a.tally_count());
	return polls_with_votes;
};
