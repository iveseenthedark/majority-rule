export default class Poll {
	constructor(key, { name, options, is_public, allow_new, owner, created, modified }, votes) {
		this.key = key;
		this.name = name;
		this.options = options;

		this.is_public = is_public;
		this.allow_new = allow_new;

		this.owner = owner;
		this.created = created;
		this.modified = modified;

		this.votes = votes || {};
	}

	winner() {
		const tallys = this.tallys();
		const max = Math.max(...tallys.map(t => t.tally));
		return tallys.filter(t => t.tally === max);
	}

	tallys() {
		const votes = Object.values(this.votes);
		return Object.entries(this.options)
			.reduce((freqs, [key, option]) => [...freqs, { key, option, tally: votes.filter(v => v === key).length }], {})
			.sort((a, b) => b.tally - a.tally);
	}

	tally_count() {
		return Object.keys(this.votes).length;
	}

	option_count() {
		return this.options.length;
	}

	has_voted(uid) {
		return uid in this.votes;
	}
}
