import React from 'react';
import { Redirect } from 'react-router-dom';

import shortid from 'shortid';
import firebase from '../../Utils/firebase';

import styled from 'styled-components';
import { themeGet } from 'styled-system';

import { Box, Label, Heading, Input } from 'rebass';
import { RadioGroup, Radio } from 'react-radio-group';

import Button from '../../Components/Button';
import Card from '../../Components/Card';
import { Table, Tbody, TR, TD as TDBase } from '../../Components/Table';

import Query from '../../Components/Query';
import GET_POLLS from '../../Actions/GET_POLL';

const TD = styled(TDBase)`
	label {
		display: block;
		margin: -1px;
	}
	input[type='radio'] {
		display: none;
		&:checked {
			~ label {
				background-color: ${themeGet('colors.teal')};
			}
		}
	}
`;

const PollVote = ({ poll, onSubmit, onChange, onSelect, selected, custom }) => (
	<Card width={[4 / 5, 3 / 4, 1 / 2]} mx="auto" p={3}>
		<Heading children={poll.name} textAlign="center" fontSize={3} />
		<form onSubmit={onSubmit}>
			<RadioGroup name="option" selectedValue={selected} onChange={onSelect}>
				<Table my={3} width={1}>
					<Tbody>
						{Object.entries(poll.options).map(([key, value]) => (
							<TR key={key}>
								<TD>
									<Radio id={key} name="radio" value={key} />
									<Label p={3} htmlFor={key}>
										{value}
									</Label>
								</TD>
							</TR>
						))}
					</Tbody>
				</Table>
			</RadioGroup>
			{poll.allow_new && (
				<Box my={2}>
					<Label>Or suggest your own</Label>
					<Input name="custom" value={custom} onChange={onChange} />
				</Box>
			)}
			<Button my={2} type="submit" disabled={!selected && !custom}>
				Vote
			</Button>
		</form>
	</Card>
);

export default class PollView extends React.Component {
	state = {
		selected: null,
		custom: ''
	};

	handleSelect = selected => {
		this.setState({ selected, custom: '' });
	};

	handleChange = ({ target: { name, value } }) => {
		this.setState({ [name]: value, selected: false });
	};

	handleSubmit = async (evt, poll) => {
		evt.preventDefault();

		let { selected, custom } = this.state;
		const { user, history } = this.props;
		const db = firebase.database();

		if (custom) {
			selected = shortid.generate();
			poll.options[selected] = custom;
			await db.ref(`polls/${poll.key}/options`).update(poll.options);
		}

		await db.ref(`votes/${poll.key}/${user.uid}`).set(selected);
		history.push(`/result/${poll.key}`);
	};

	render = () => {
		const { match, user } = this.props;
		const { selected, custom } = this.state;

		return (
			<Query action={GET_POLLS} variables={{ key: match.params.key }}>
				{poll =>
					poll.has_voted(user.uid) ? (
						<Redirect to={`/result/${match.params.key}`} />
					) : (
						<PollVote
							poll={poll}
							custom={custom}
							selected={selected}
							onSelect={this.handleSelect}
							onChange={this.handleChange}
							onSubmit={evt => this.handleSubmit(evt, poll)}
						/>
					)
				}
			</Query>
		);
	};
}
