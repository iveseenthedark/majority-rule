import React from 'react';
import { Flex, Container } from 'rebass';

import PollCard from '../../Components/PollCard';

import Query from '../../Components/Query';
import LIST_POLLS from '../../Actions/LIST_POLLS';

export default props => (
	<Flex flexWrap="wrap" justifyContent="center" mx="auto">
		<Query action={LIST_POLLS}>{polls => polls.map(poll => <PollCard key={poll.key} poll={poll} />)}</Query>
	</Flex>
);
