import React from 'react';
import { Flex } from 'rebass';

import PollCard from '../../Components/PollCard';

import Query from '../../Components/Query';
import MY_POLLS from '../../Actions/MY_POLLS';

export default props => (
	<Flex flexWrap="wrap" justifyContent="center">
		<Query action={MY_POLLS}>{polls => polls.map(poll => <PollCard key={poll.key} poll={poll} />)}</Query>
	</Flex>
);
