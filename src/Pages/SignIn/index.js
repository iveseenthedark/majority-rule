import React from 'react';

import { Box, Input, Text, Label } from 'rebass';

import ActivityIndicator from '../../Components/ActivityIndicator';
import IconBadge from '../../Components/IconBadge';
import Card from '../../Components/Card';
import Button from '../../Components/Button';

import isEmail from 'validator/lib/isEmail';
import { auth, actionCodeSettings } from '../../Utils/firebase';

const SignInForm = ({ email, isValid, onSubmit, onChange }) => (
	<Box>
		<Text my={5} align="center" color="black">
			<IconBadge my={3} mx="auto" icon="sign-in-alt" />
			<p>Enter your email address and we'll send you a link you can use to sign in</p>
		</Text>

		<form onSubmit={onSubmit}>
			<Label>Email</Label>
			<Input name="email" value={email} type={email} placeholder="e.g., threepwood@grogmail.com" onChange={onChange} />
			<Button width={1} mt={3} type="submit" disabled={!isValid}>
				Login
			</Button>
		</form>
	</Box>
);

const WaitForEmail = () => (
	<Box>
		<Text my={5} align="center" color="black">
			<IconBadge my={4} mx="auto" icon="thumbs-up" bg="teal" />
			<p>Please check your email.</p>
		</Text>
	</Box>
);

export class FinishSignUp extends React.Component {
	componentWillMount = () => {
		if (auth.isSignInWithEmailLink(window.location.href)) {
			var email = window.localStorage.getItem('emailForSignIn');
			if (!email) {
				email = window.prompt('Please provide your email for confirmation');
			}
			auth
				.signInWithEmailLink(email, window.location.href)
				.then(function(result) {
					window.localStorage.removeItem('emailForSignIn');
					console.log('Logged in');
				})
				.catch(function(error) {
					console.log(error);
				});
		}
	};

	render = () => <ActivityIndicator />;
}

export class SignIn extends React.Component {
	state = {
		email: '',
		isValid: false,
		submitted: false
	};

	handleChange = ({ target: { value } }) => {
		this.setState({
			email: value,
			isValid: isEmail(value)
		});
	};

	handleSubmit = evt => {
		evt.preventDefault();
		const { email } = this.state;

		auth
			.sendSignInLinkToEmail(email, actionCodeSettings)
			.then(() => {
				window.localStorage.setItem('emailForSignIn', email);
				this.setState({ submitted: true });
			})
			.catch(error => {
				console.log(error);
			});
	};

	render = () => {
		const { submitted } = this.state;
		return (
			<Card width={[4 / 5, 3 / 4, 1 / 2]} mx="auto" p={3}>
				{submitted ? (
					<WaitForEmail />
				) : (
					<SignInForm onChange={this.handleChange} onSubmit={this.handleSubmit} {...this.state} />
				)}
			</Card>
		);
	};
}
