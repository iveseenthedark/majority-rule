import React from 'react';
import shortid from 'shortid';
import firebase from '../../Utils/firebase';

import { Box, Input, Label, Flex, Dot, Switch } from 'rebass';
import Card from '../../Components/Card';
import Button from '../../Components/Button';

export default class NewPollPage extends React.Component {
	state = {
		name: '',
		options: { [shortid.generate()]: '' },
		is_public: true,
		allow_new: true,
		stepIndex: 0
	};

	/* */
	handleNext = () => {
		this.setState(({ stepIndex }) => ({ stepIndex: stepIndex + 1 }));
	};

	/* */
	handlePrev = () => {
		this.setState(({ stepIndex }) => ({ stepIndex: stepIndex - 1 }));
	};

	/* */
	handleChange = ({ target: { name, value } }) => {
		this.setState({ [name]: value });
	};

	/* */
	handleOptionChanged = ({ target: { name, value } }, add_new) => {
		this.setState(({ options }) => {
			const new_options = { ...options, [name]: value };
			if (add_new) {
				new_options[shortid.generate()] = '';
			}
			return { options: new_options };
		});
	};

	/* */
	handleOptionRemove = () => {
		this.setState(({ options }) => {
			Object.entries(options).forEach(([key, value], idx, { length }) => {
				// Remove all the blank entries that aren't the last one
				idx !== length - 1 && (value === null || value === '') && delete options[key];
			});
			return { options };
		});
	};

	handleSubmit = evt => {
		evt.preventDefault();
		const { name, options, is_public, allow_new } = this.state;
		const { user } = this.props;

		// Clear blank options
		Object.entries(options).forEach(([key, value]) => {
			(value === null || value === '') && delete options[key];
		});

		const poll = {
			name,
			is_public,
			allow_new,
			options,
			owner: user.uid,
			created: firebase.database.ServerValue.TIMESTAMP,
			modified: firebase.database.ServerValue.TIMESTAMP
		};

		firebase
			.database()
			.ref('polls')
			.push(poll)
			.then(data => {
				this.props.history.replace(`/vote/${data.key}`);
			});
	};

	render = () => {
		const { stepIndex, name, options, is_public, allow_new } = this.state;
		return (
			<Card width={[4 / 5, 3 / 4, 1 / 2]} mx="auto" p={3} noanimation="true">
				<Flex justifyContent="center" my={3}>
					<Dot bg={stepIndex === 0 ? 'black' : 'gray'} />
					<Dot bg={stepIndex === 1 ? 'black' : 'gray'} />
					<Dot bg={stepIndex === 2 ? 'black' : 'gray'} />
				</Flex>

				<form onSubmit={this.handleSubmit}>
					{stepIndex === 0 && (
						<Box>
							<Label htmlFor="name">What's the Poll about?</Label>
							<Input name="name" value={name} onChange={this.handleChange} />
							<Button disabled={name === ''} mt={3} width={1} onClick={this.handleNext}>
								Next
							</Button>
						</Box>
					)}

					{stepIndex === 1 && (
						<Box>
							<Label>Poll Options</Label>
							{Object.entries(options).map(([key, value], idx, { length }) => (
								<Input
									my={1}
									key={key}
									name={key}
									value={value}
									onBlur={this.handleOptionRemove}
									onChange={evt => this.handleOptionChanged(evt, idx === length - 1)}
								/>
							))}
							<Button disabled={options.length <= 1} mt={3} width={1} onClick={this.handleNext}>
								Next
							</Button>
						</Box>
					)}

					{stepIndex === 2 && (
						<Box>
							<Flex justifyContent="flex-start" mb={2}>
								<Switch
									mr={3}
									checked={is_public}
									onClick={() => this.setState(({ is_public }) => ({ is_public: !is_public }))}
								/>
								<Label>Public</Label>
							</Flex>
							<Flex justifyContent="flex-start">
								<Switch
									mr={3}
									checked={allow_new}
									onClick={() => this.setState(({ allow_new }) => ({ allow_new: !allow_new }))}
								/>
								<Label>Allow New Options</Label>
							</Flex>
							<Button mt={3} width={1} type="submit">
								Save
							</Button>
						</Box>
					)}

					{stepIndex > 0 && (
						<Button mt={1} width={1} bg="gray" color="black" onClick={this.handlePrev}>
							Back
						</Button>
					)}
				</form>
			</Card>
		);
	};
}
