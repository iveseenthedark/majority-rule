import React from 'react';
import { Redirect } from 'react-router-dom';

import styled from 'styled-components';
import { Badge, Box, Flex, Lead, Text, ButtonTransparent } from 'rebass';
import { TD, TR, Table, Tbody } from '../../Components/Table';

import IconBadge from '../../Components/IconBadge';
import CardBase from '../../Components/Card';
import ReactCardFlip from 'react-card-flip';

import { ColourScale } from '../../Utils/chart';
import { VictoryPie, VictoryLabel, VictoryContainer } from 'victory';

import Query from '../../Components/Query';
import GET_POLLS from '../../Actions/GET_POLL';

/* */
const Card = styled(CardBase)`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	height: 500px;
`;

/* */
const join_options = (options, conjunction = '&', oxford_comma = true) => {
	const text = options.map(o => o.option);
	switch (text.length) {
		case 0: {
			return '';
		}
		case 1: {
			return text[0];
		}
		case 2: {
			return `${text[0]} ${conjunction} ${text[1]}`;
		}
		default: {
			return `${text.slice(0, -1).join(', ')}${oxford_comma ? ',' : ''} ${conjunction} ${text.slice(-1)}`;
		}
	}
};

/* */
const SingleWinner = ({ winner }) => (
	<React.Fragment>
		The consensus is: <br />
		<strong>{winner.option}</strong>
	</React.Fragment>
);

/* */
const MultipleWinners = ({ winners }) => (
	<React.Fragment>
		Opinion is split between: <br />
		<strong>{join_options(winners)}</strong>
	</React.Fragment>
);

/* */
const ResultPanel = ({ poll, winners }) => (
	<Flex flexDirection="column" alignItems="center">
		<IconBadge my={4} icon={winners.length > 1 ? 'comments' : 'comment'} />
		<Lead fontSize={5} textAlign="center">
			{poll.name}
		</Lead>
		<Text my={3} fontSize={3} textAlign="center">
			{winners.length > 1 ? <MultipleWinners winners={winners} /> : <SingleWinner winner={winners[0]} />}
		</Text>
	</Flex>
);

/* */
const StatsPanel = ({ poll }) => {
	const chart_data = poll
		.tallys()
		.map(({ option, tally }) => ({ x: [option], y: tally }))
		.filter(datum => datum.y > 0);

	return (
		<Flex alignItems="center" flexWrap="wrap">
			<Box mx={[4, 'auto']} width={[1, 1 / 2]}>
				<VictoryContainer width={400} height={400}>
					<VictoryPie
						standalone={false}
						data={chart_data}
						colorScale={ColourScale}
						innerRadius={100}
						padAngle={3}
						labels={() => null}
					/>
					<VictoryLabel
						textAnchor="middle"
						style={{ fontSize: 20 }}
						x={200}
						y={200}
						text={`${poll.tally_count()}\nVotes`}
					/>
				</VictoryContainer>
			</Box>
			<Table width={[1, 1 / 2]}>
				<Tbody>
					{poll.tallys().map(({ key, option, tally }, idx) => (
						<TR key={key}>
							<TD px={3} py={2}>
								{option}
							</TD>
							<TD px={3} py={2} textAlign="right">
								<Badge bg={tally > 0 ? ColourScale[idx] : 'gray'}>{tally}</Badge>
							</TD>
						</TR>
					))}
				</Tbody>
			</Table>
		</Flex>
	);
};

/* */
const PollResults = ({ poll, isFlipped, hasFlipped, onFlip }) => {
	const card_props = { width: [4 / 5, 3 / 4, 1 / 2], mx: 'auto', p: 3 };
	return (
		<ReactCardFlip isFlipped={isFlipped}>
			<Card key="front" {...card_props}>
				<Box px={1} css={{ overflow: 'scroll', textAlign: 'center' }}>
					<ResultPanel poll={poll} winners={poll.winner()} />
					<ButtonTransparent my={3} onClick={onFlip}>
						View Stats
					</ButtonTransparent>
				</Box>
			</Card>
			<Card key="back" {...card_props}>
				<Box px={1} css={{ overflow: 'scroll', textAlign: 'center', display: hasFlipped ? 'block' : 'none' }}>
					<StatsPanel poll={poll} winners={poll.winner()} />
					<ButtonTransparent my={3} onClick={onFlip}>
						Back to Results
					</ButtonTransparent>
				</Box>
			</Card>
		</ReactCardFlip>
	);
};

export default class PollView extends React.Component {
	state = { isFlipped: false, hasFlipped: false };

	handleFlip = () => {
		this.setState(({ isFlipped }) => ({ isFlipped: !isFlipped, hasFlipped: true }));
	};

	render = () => {
		const { match, user } = this.props;
		const { isFlipped, hasFlipped } = this.state;

		return (
			<Query action={GET_POLLS} variables={{ key: match.params.key }}>
				{poll =>
					!poll.has_voted(user.uid) ? (
						<Redirect to={`/vote/${match.params.key}`} />
					) : (
						<PollResults poll={poll} isFlipped={isFlipped} hasFlipped={hasFlipped} onFlip={this.handleFlip} />
					)
				}
			</Query>
		);
	};
}
