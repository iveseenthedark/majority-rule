import React from 'react';
import { Circle } from 'rebass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default ({ icon, size = 150, fontSize = '6x', ...props }) => (
	<Circle size={size} css={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} {...props}>
		<FontAwesomeIcon icon={icon} size={fontSize} />
	</Circle>
);
