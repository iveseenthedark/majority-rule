import styled, { keyframes } from 'styled-components';
import { themeGet, width, height, flex } from 'styled-system';
import { Card } from 'rebass';

const FadeIn = keyframes`
from {
    opacity: 0;
    transform: translate3d(0, 2rem, 0);
  }

  to {
    opacity: 1;
    transform: translate3d(0, 0, 0);
  }
`;

export default styled(Card)`
	${width};
	${height};
  ${flex}
	margin-top: ${themeGet('space.3')}px;
	animation: ${props => (props.noanimation ? 'none' : `${FadeIn} 800ms cubic-bezier(0.15, 1, 0.33, 1)`)};
`;
