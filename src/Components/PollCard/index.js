import React from 'react';

import Card from '../Card';
import styled from 'styled-components';
import { Flex, BlockLink, Text, Box, Badge } from 'rebass';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const PollCard = styled(Card)`
	width: 300px;
	min-height: 100px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;

export default ({ poll }) => (
	<BlockLink href={`#/vote/${poll.key}`}>
		<PollCard m={3} p={3}>
			<Flex alignItems="baseline">
				<Box fontSize={1} bg="blue" color="white" p={1}>
					<FontAwesomeIcon icon="balance-scale" />
				</Box>
				<Text fontWeight={500} fontSize={2} ml={2}>
					{poll.name}
				</Text>
			</Flex>
			<Flex justifyContent="flex-end">
				<Text>
					<Badge bg="violet" mx={0}>
						Votes: {poll.tally_count()}
					</Badge>
				</Text>
			</Flex>
		</PollCard>
	</BlockLink>
);
