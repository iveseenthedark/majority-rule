import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Auth from '../Auth';

/**
 * This reoute will redirect to you the login screen if you're not authenticated
 */
export const SecureRoute = ({ component: Component, user, ...rest }) => (
	<Auth>
		{user =>
			user ? <Route {...rest} children={props => <Component user={user} {...props} />} /> : <Redirect to="/signin" />
		}
	</Auth>
);

/**
 * This route will redirect you to the homepage if you've authenticated in already
 */
export const AuthRoute = ({ component: Component, user, ...rest }) => (
	<Auth>{user => (user ? <Redirect to="/" /> : <Route {...rest} children={props => <Component {...props} />} />)}</Auth>
);
