import React from 'react';
import { Route } from 'react-router-dom';

import { Toolbar as Base, Caps, Box, BlockLink } from 'rebass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import styled from 'styled-components';

const Toolbar = styled(Base)`
	top: 0;
	width: 100%;
	z-index: ${props => props.zindex};
	position: fixed;
`;

const HiddenOnHomeLink = props => (
	<Route
		path="/"
		exact
		children={({ match, history }) => {
			return props.drawopen || match ? (
				<Box {...props} />
			) : (
				<BlockLink onClick={() => history.goBack()} {...props}>
					<FontAwesomeIcon icon="chevron-left" />
				</BlockLink>
			);
		}}
	/>
);

export default ({ drawOpen, onToggleDraw }) => {
	const toolbar_icon = drawOpen ? 'times' : 'bars';
	return (
		<header>
			<Toolbar bg="blue" zindex={100} />
			<Toolbar bg="transparent" px={3} zindex={300}>
				<HiddenOnHomeLink width="15px" drawopen={drawOpen ? 1 : 0} />
				<Caps mx="auto">Majority Rule</Caps>
				<Box width="15px">
					<FontAwesomeIcon onClick={onToggleDraw} icon={toolbar_icon} />
				</Box>
			</Toolbar>
		</header>
	);
};
