import React from 'react';
import { Box, Text, BlockLink, Drawer as Base } from 'rebass';

import styled from 'styled-components';

const Drawer = styled(Base)`
	width: 100%;
	z-index: 200;
	background-image: linear-gradient(to right bottom, #00ee87, #00df98, #00cea3, #00bda8, #00aca6);
`;

export default props => (
	<Drawer is="aside" side="right" color="white" {...props}>
		<Box pt={5} px={3}>
			<BlockLink href="#/new">
				<Box px="auto" py={4}>
					<Text textAlign="center" fontWeight="bold" fontSize={5} children="New" />
				</Box>
			</BlockLink>
			<BlockLink href="#/mine">
				<Box px="auto" py={4}>
					<Text textAlign="center" fontWeight="bold" fontSize={5} children="My Polls" />
				</Box>
			</BlockLink>
		</Box>
	</Drawer>
);
