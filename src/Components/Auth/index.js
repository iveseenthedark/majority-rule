import React from 'react';

import { auth } from '../../Utils/firebase';
import ActivityIndicator from '../ActivityIndicator';

export default class Auth extends React.Component {
	state = { user: null, loaded: false };

	componentWillMount = () => {
		this.setState({ loading: true });
		auth.onAuthStateChanged(user => {
			if (user) {
				this.setState({ user });
			}
			this.setState({ loading: false });
		});
	};

	render = () => {
		const { children } = this.props;
		const { user, loading } = this.state;
		return loading ? <ActivityIndicator /> : children(user);
	};
}
