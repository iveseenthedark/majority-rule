import styled from 'styled-components';
import { Button } from 'rebass';

export default styled(Button)`
	width: 100%;
`;
