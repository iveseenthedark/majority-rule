import styled from 'styled-components';
import { themeGet, space, color, fontSize, width, textAlign } from 'styled-system';

export const Table = styled.table`
	${space};
	${color};
	${width};
	${fontSize};

	overflow: hidden;
	border-collapse: collapse;
	border-radius: ${themeGet('radii.small', '4px')};
	box-shadow: ${themeGet('shadows.1')};
`;

export const Tbody = styled.tbody`
	${space};
	${color};
	${width};
	${fontSize};
`;

export const TR = styled.tr`
	${space};
	${color};
	${width};
	${fontSize};

	border-bottom: 1px solid ${themeGet('colors.gray')};
	&:last-child {
		border: none;
	}
`;

export const TD = styled.td`
	${space};
	${color};
	${width};
	${fontSize};
	${textAlign};
`;
