import {injectGlobal} from 'styled-components';

export default injectGlobal`
* { box-sizing: border-box; }

html, body { 
	margin: 0; 
	padding: 0; 
	background-color: #0067ee;
	height: 100%;
	line-height: 1.33;
}

#root{
	height:100%;
}
`;
