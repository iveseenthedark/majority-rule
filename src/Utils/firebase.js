import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import config from '../firebase.config.json';
import app from '../../package.json';

firebase.initializeApp(config);

/* */
export const auth = firebase.auth();

/* */
const homepage = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : app.homepage;
export const actionCodeSettings = { url: `${homepage}/#/fsu`, handleCodeInApp: true };

/* */
export default firebase;
