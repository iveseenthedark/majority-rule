import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faBars,
	faBalanceScale,
	faQuestion,
	faTimes,
	faCircle,
	faCheck,
	faChevronLeft,
	faChevronRight,
	faThumbsUp,
	faSignInAlt,
	faComment,
	faComments
} from '@fortawesome/free-solid-svg-icons';

library.add(
	faBars,
	faTimes,
	faCheck,
	faCircle,
	faComment,
	faComments,
	faThumbsUp,
	faQuestion,
	faSignInAlt,
	faChevronLeft,
	faChevronRight,
	faBalanceScale
);
