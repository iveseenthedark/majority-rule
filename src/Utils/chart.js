export const ColourScale = [
	'#00deee',
	'#00ee87',
	'#deee00',
	'#ee00de',
	'#00ee10',
	'#ee0067',
	'#67ee00',
	'#0067ee',
	'#ee1000',
	'#8700ee',
	'#1000ee'
];
